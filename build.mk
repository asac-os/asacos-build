#!/usr/bin/make -f
#
# Copyright (C) 2012  Alexander Sack <asac@jwsdot.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#


ifneq (,$(DESTDIR)$(O))
$(error don't set DESTDIR or O; this will make asacos system build break. You probably want to use 'make ... OO=<OUTPUT-DIR>'. Have fun, good bye!)
endif

include $(CURDIR)/asacos-build/common.mk

# source all beggs with reset before and exec.mk after
include $(foreach a,$(wildcard $(CURDIR)/asacos-build/beggs/*.mk), \
	$(CURDIR)/asacos-build/reset.mk \
	$(a) \
	$(CURDIR)/asacos-build/exec.mk)

# provide a spin all modules target
spin-all: $(foreach a,$(ASACOS_MODULES),$(OO)$(a)-install-stamp)

all: spin-all

$(foreach a,$(ASACOS_MODULES),$(a)-build): %-build: out/%-build-stamp

$(foreach a,$(ASACOS_MODULES),$(a)-install): %-install: out/%-install-stamp

echovars: $(foreach a,$(ASACOS_MODULES),$(a)-echo-vars)

clean:
	rm -rf $(foreach p,$(sort $(ASACOS_PRODUCTS)),$(OO)$(p)/)
	rm -f $(OO)*-stamp

.PHONY: spin echovars clean $(foreach a,$(ASACOS_MODULES),$(a)-build) $(foreach a,$(ASACOS_MODULES),$(a)-install)

