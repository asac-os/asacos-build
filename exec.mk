#!/usr/bin/make -f
#
# Copyright (C) 2012  Alexander Sack <asac@jwsdot.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

ASACOS_MODULES := $(ASACOS_MODULES) $(MODULE)
ASACOS_PRODUCTS := $(ASACOS_PRODUCTS) $(PRODUCT)

# where do we put stuff:
# - out/host/OBJs/$(host)/ - the host build objects
# - out/host/$(MODULE_PRODUCT)/$(host)/ - the toolchain root
# - out/target/OBJS/$(target)/ - the host build objects
# - out/target/$(MODULE_PRODUCT)/$(target)/ - a native toolchain for the target

$(MODULE)_TSYSROOT := $(ABS_O)/toolchain/$(shell echo $(BUILD) | sed -e 's/^\([^-]*-[^-]*\).*$$/\1/')/sysroot
$(MODULE)_SYSROOT := $(ABS_O)/$(PRODUCT)/$(shell echo $(BUILD) | sed -e 's/^\([^-]*-[^-]*\).*$$/\1/')
$(MODULE)_XTOOLS := $($(MODULE)_TSYSROOT)/..

ifeq (host,$(CLASS))
$(MODULE)_DEST := $(OO)$(PRODUCT)/OBJs__$(shell echo $(BUILD) | sed -e 's/^\([^-]*-[^-]*\).*$$/\1/')/$(MODULE)/
else
ifeq (target,$(CLASS))
$(MODULE)_DEST := $(OO)$(PRODUCT)/OBJs__$(shell echo $(BUILD) | sed -e 's/^\([^-]*-[^-]*\).*$$/\1/')/$(MODULE)/
else
ifeq (NOP,$(CLASS))
$(MODULE)_DEST :=
else
$(error unrecognized MODULE_CLASS+CAT combination $(CLASS)-$(CAT))
endif
endif
endif

$(MODULE)_mod := $(MODULE)
$(MODULE)_O := $(OO)
$(MODULE)_PRODUCT := $(PRODUCT)
$(MODULE)_CLASS := $(CLASS)
$(MODULE)_CAT := $(CAT)
$(MODULE)_TYPE := $(CAT)
$(MODULE)_SRC_NAME := $(SRC_NAME)
$(MODULE)_SRC := $(CURDIR)/$(SRC_NAME)
$(MODULE)_REQ := $(REQ)
$(MODULE)_HOST := $(HOST)
$(MODULE)_BUILD := $(BUILD)
$(MODULE)_TARGET := $(TARGET)
$(MODULE)_CONFIGURE_FLAGS := $(CONFIGURE_FLAGS)
$(MODULE)_MAKE_TARGET := $(MAKE_TARGET)
$(MODULE)_MAKE_INSTALL_TARGET := $(MAKE_INSTALL_TARGET)
$(MODULE)_C_ENVS := $(C_ENVS)
$(MODULE)_M_ARGS := $(M_ARGS)
$(MODULE)_M_VARS := $(M_VARS)
$(MODULE)_MACHINE_ARCH := $(MACHINE_ARCH)
$(MODULE)_MI_ARGS := $(MI_ARGS)
$(MODULE)_MI_VARS := $(MI_VARS)
$(MODULE)_CUSTOM_MAKEINSTALL_ARGS := $(CUSTOM_MAKEINSTALL_ARGS)

expand = $($(firstword $(subst -, ,$(subst $(OO),,$(1))))_$(2))

$(MODULE)-echo-vars:
	@echo === START build for: $(call expand,$@,mod) ===
	@echo REQ: $(call expand,$@,REQ)
	@echo SRC: $(call expand,$@,SRC)
	@echo O: $(call expand,$@,O)
	@echo DEST: $(call expand,$@,DEST)
	@echo SYSROOT: $(call expand,$@,SYSROOT)
	@echo CONFIGURE_FLAGS $(call expand,$@,CONFIGURE_FLAGS)
	@echo ---

# ---
# rule for CLASS: NOP
# + NOP is a pure dependency begg that does nothing but exec the REQs
# + XXX: document available hooks here
# ---
ifeq (NOP,$(CLASS))
$(OO)$(MODULE)-build-stamp: $(foreach i,$(REQ),$(OO)$(i)-install-stamp)
	touch $@

$(OO)$(MODULE)-install-stamp: $(foreach i,$(REQ),$(OO)$(i)-install-stamp)
	touch $@
endif

# ---
# rule for: host-xtools-autoconf
# + XXX: document available hooks here
# ---
ifeq (host-xtoolchain-autoconf,$(CLASS)-$(CAT)-$(TYPE))
$(OO)$(MODULE)-configure-stamp: $(foreach i,$(REQ),$(OO)$(i)-install-stamp)
	mkdir -p $(call expand,$@,DEST) || true; \
	cd $(call expand,$@,DEST); \
		PATH=$(call expand,$@,XTOOLS)/usr/bin:$$PATH \
		LD_LIBRARY_PATH=$(call expand,$@,XTOOLS)/usr/lib \
			$(call expand,$@,C_ENVS) $(call expand,$@,SRC)/configure \
				--prefix=$(call expand,$@,XTOOLS)/usr \
				--build=$(call expand,$@,BUILD) \
				--host=$(call expand,$@,HOST) \
				--target=$(call expand,$@,TARGET) \
				--with-sysroot=$(call expand,$@,TSYSROOT) \
				$(subst __SYSROOT__,$(call expand,$@,TSYSROOT),$(call expand,$@,CONFIGURE_FLAGS))
	touch $@

$(OO)$(MODULE)-build-stamp: $(OO)$(MODULE)-configure-stamp
	cd $(call expand,$@,DEST); \
		PATH=$(call expand,$@,XTOOLS)/usr/bin:$$PATH \
		LD_LIBRARY_PATH=$(call expand,$@,XTOOLS)/usr/lib \
		$(call expand,$@,M_VARS) \
			make -j$(exec_MAKEFLAGS_J) $(call expand,$@,MAKE_TARGET) $(call expand,$@,M_ARGS)
	touch $@

$(OO)$(MODULE)-install-stamp: $(OO)$(MODULE)-build-stamp $(foreach i,$(REQ),$(OO)$(i)-install-stamp)
	cd $(call expand,$@,DEST); \
		PATH=$(call expand,$@,XTOOLS)/usr/bin:$$PATH \
		LD_LIBRARY_PATH=$(call expand,$@,XTOOLS)/usr/lib \
		$(call expand,$@,MI_VARS) \
			$(call expand,$@,C_ENVS) fakeroot make $(call expand,$@,MAKE_INSTALL_TARGET) $(call expand,$@,MI_ARGS)
	touch $@
endif


# ---
# rule for: host-tools-autoconf
# + XXX: document available hooks here
# ---
ifeq (host-xtools-autoconf,$(CLASS)-$(CAT)-$(TYPE))
$(OO)$(MODULE)-configure-stamp: $(foreach i,$(REQ),$(OO)$(i)-install-stamp)
	mkdir -p $(call expand,$@,DEST) || true; \
	cd $(call expand,$@,DEST); \
		PATH=$(call expand,$@,XTOOLS)/usr/bin:$$PATH \
		LD_LIBRARY_PATH=$(call expand,$@,XTOOLS)/usr/lib \
		$(call expand,$@,C_ENVS) \
			$(call expand,$@,SRC)/configure \
				--prefix=$(call expand,$@,XTOOLS)/usr \
				--build=$(call expand,$@,BUILD) \
				--host=$(call expand,$@,HOST) \
				$(subst __SYSROOT__,$(call expand,$@,TSYSROOT),$(call expand,$@,CONFIGURE_FLAGS))
	touch $@

$(OO)$(MODULE)-build-stamp: $(OO)$(MODULE)-configure-stamp
	cd $(call expand,$@,DEST); \
		PATH=$(call expand,$@,XTOOLS)/usr/bin:$$PATH \
		LD_LIBRARY_PATH=$(call expand,$@,XTOOLS)/usr/lib \
		$(call expand,$@,M_VARS) \
			make -j$(exec_MAKEFLAGS_J) $(call expand,$@,MAKE_TARGET) $(call expand,$@,M_ARGS)
	touch $@

$(OO)$(MODULE)-install-stamp: $(OO)$(MODULE)-build-stamp
	cd $(call expand,$@,DEST); \
		PATH=$(call expand,$@,XTOOLS)/usr/bin:$$PATH \
		$(call expand,$@,MI_VARS) \
		LD_LIBRARY_PATH=$(call expand,$@,XTOOLS)/usr/lib \
			fakeroot make $(call expand,$@,MAKE_INSTALL_TARGET) $(call expand,$@,MI_ARGS)
	touch $@

endif


# ---
# rule for: target-tools-autoconf
# + XXX: document available hooks here
# ---
ifeq (target-tools-autoconf,$(CLASS)-$(CAT)-$(TYPE))
$(OO)$(MODULE)-configure-stamp: $(foreach i,$(REQ),$(OO)$(i)-install-stamp)
	mkdir -p $(call expand,$@,DEST) || true; \
	cd $(call expand,$@,DEST); \
		 PATH=$(call expand,$@,XTOOLS)/usr/bin:$$PATH \
		 LD_LIBRARY_PATH=$(call expand,$@,XTOOLS)/usr/lib \
                 $(call expand,$@,C_ENVS) \
                    $(call expand,$@,SRC)/configure \
			--prefix=/usr \
			--build=$(call expand,$@,BUILD) \
			--host=$(call expand,$@,HOST) \
			--target=$(call expand,$@,TARGET) \
			$(subst __SYSROOT__,$(call expand,$@,TSYSROOT),$(call expand,$@,CONFIGURE_FLAGS))
	touch $@

$(OO)$(MODULE)-build-stamp: $(OO)$(MODULE)-configure-stamp
	cd $(call expand,$@,DEST); \
		PATH=$(call expand,$@,XTOOLS)/usr/bin:$$PATH \
		LD_LIBRARY_PATH=$(call expand,$@,XTOOLS)/usr/lib \
		$(call expand,$@,M_VARS) \
			make -j$(exec_MAKEFLAGS_J) $(call expand,$@,MAKE_TARGET) $(call expand,$@,M_ARGS)
	touch $@

$(OO)$(MODULE)-install-stamp: $(OO)$(MODULE)-build-stamp 
	cd $(call expand,$@,DEST); \
		PATH=$(call expand,$@,XTOOLS)/usr/bin:$$PATH \
		LD_LIBRARY_PATH=$(call expand,$@,XTOOLS)/usr/lib \
		$(call expand,$@,MI_VARS) \
			fakeroot make $(call expand,$@,MAKE_INSTALL_TARGET) DESTDIR=$(call expand,$@,TSYSROOT) $(call expand,$@,MI_ARGS)
	touch $@

endif


# ---
# rule for: target-kernelheaders-make
# + XXX: document available hooks here
# ---
ifeq (target-kernelheaders-make,$(CLASS)-$(CAT)-$(TYPE))
# generic rule for MODULE_TYPE=host-kernelheaders-make
$(OO)$(MODULE)-build-stamp: $(foreach i,$(REQ),$(OO)$(i)-install-stamp)
	cd $(call expand,$@,SRC); \
		make mrproper headers_check headers_install ARCH=$(call expand,$@,MACHINE_ARCH) INSTALL_HDR_PATH=$(call expand,$@,TSYSROOT)/usr
	touch $@

$(OO)$(MODULE)-install-stamp: $(OO)$(MODULE)-build-stamp $(foreach i,$(REQ),$(OO)$(i)-install-stamp)
	touch $@

endif # ifeq (target-kernelheaders-make,$(CLASS)-$(CAT)-$(TYPE))

.PHONY: $(MODULE)-echo-vars
.NOTPARALLEL: $(OO)$(MODULE)-build-stamp $(OO)$(MODULE)-install-stamp
