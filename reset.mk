#!/usr/bin/make -f
#
# Copyright (C) 2012  Alexander Sack <asac@jwsdot.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

# mandatory params are empty
MODULE :=
PRODUCT :=
CLASS :=
CAT :=
TYPE :=

# optional params have defaults (even empty ones)
C_ENVS :=
M_VARS :=
MI_VARS :=
M_ARGS :=
MI_ARGS :=
SRC_NAME :=
CONFIGURE_FLAGS :=
MAKE_TARGET :=
MAKE_INSTALL_TARGET := install
REQ :=

BUILD := $(shell $(CURDIR)/asacos-build/config.guess | sed -e 's/-[^-]*/-cross/')
HOST := $(shell $(CURDIR)/asacos-build/config.guess | sed -e 's/-[^-]*/-cross/')

