#!/usr/bin/make -f
#
# Copyright (C) 2012  Alexander Sack <asac@jwsdot.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
MODULE		:= glibc_1_i586_elf
SRC_NAME	:= glibc
PRODUCT		:= toolchain

# architecture info
TARGET		:= i586-pc-linux-gnu
HOST		:= i586-pc-linux-gnu

# build system type triplet
CLASS		:= target
CAT		:= tools
TYPE		:= autoconf

# REQ: MODULEs required before building this one
REQ 		:= 					\
	linux_headers_i586_elf				\
	gcc_1_i586_elf					\
	$(NULL)

# C_ENVS: set environment variables for configure stage
C_ENVS		:=					\
	CFLAGS="-march=i586 -mtune=generic -g -O2"	\
	$(NULL)

# M_ARGS: set variables passed to make stage
M_ARGS		:=					\
	gnulib="-lgcc"					\
	static-gnulib="-lgcc"				\
	libc.so-gnulib="-lgcc"				\
	$(NULL)

# CONFIGURE_FLAGS: set extra configure flags
CONFIGURE_FLAGS	:=					\
    --disable-profile					\
    --enable-add-ons=../glibc-ports,nptl		\
    --enable-kernel=2.6.25				\
    --with-tls --with-__thread				\
    --with-binutils=__SYSROOT__/../usr/bin	\
    --with-headers=__SYSROOT__/usr/include	\
    libc_cv_ctors_header=yes				\
    libc_cv_c_cleanup=yes				\
    libc_cv_z_relro=yes					\
    libc_cv_ssp=no					\
    $(NULL)

include $(CURDIR)/asacos-build/exec.mk
MODULE		:= glibc_1_arm_eabi
TARGET		:= arm-asac-linux-gnueabi
HOST		:= arm-asac-linux-gnueabi

C_ENVS		:=					\
	CFLAGS="-march=armv7-a -mtune=cortex-a8 -g -O2"	\
	$(NULL)

REQ 		:= 					\
	linux_headers_arm_eabi				\
	gcc_1_arm_eabi					\
	$(NULL)


include $(CURDIR)/asacos-build/exec.mk
MODULE		:= glibc_2_i586_elf
SRC_NAME	:= glibc
PRODUCT		:= toolchain

# architecture info
TARGET		:= i586-pc-linux-gnu
HOST		:= i586-pc-linux-gnu

# build system type triplet
CLASS		:= target
CAT		:= tools
TYPE		:= autoconf

# REQ: MODULEs required before building this one
REQ 		:= 					\
		gcc_2_i586_elf				\
	$(NULL)

# C_ENVS: set environment variables for configure stage
C_ENVS		:=					\
	CFLAGS="-march=i586 -mtune=generic -g -O2"	\
	CC=$(TARGET)-gcc				\
	$(NULL)

# M_ARGS: set variables passed to make stage
M_ARGS		:=					\
	$(NULL)

# CONFIGURE_FLAGS: set extra configure flags
CONFIGURE_FLAGS	:=					\
    --disable-profile					\
    --enable-add-ons					\
    --enable-kernel=2.6.25				\
    --with-tls --with-__thread				\
    --with-headers=__SYSROOT__/usr/include		\
    --enable-add-ons=../glibc-ports,nptl		\
    libc_cv_ctors_header=yes				\
    libc_cv_c_cleanup=yes				\
    libc_cv_z_relro=yes					\
    libc_cv_ssp=no					\
    $(NULL)


include $(CURDIR)/asacos-build/exec.mk
MODULE		:= glibc_2_arm_eabi

# architecture info
TARGET		:= arm-asac-linux-gnueabi
HOST		:= arm-asac-linux-gnueabi

REQ 		:= 					\
		gcc_2_arm_eabi				\
		$(NULL)


C_ENVS		:=					\
		$(NULL)
