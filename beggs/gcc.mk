#!/usr/bin/make -f
#
# Copyright (C) 2012  Alexander Sack <asac@jwsdot.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
MODULE := gcc_1_i586_elf
TARGET := i586-pc-linux-gnu
PRODUCT := toolchain
CLASS := host
CAT := xtoolchain
TYPE := autoconf
SRC_NAME := gcc
REQ := binutils_1_i586_elf
MAKE_TARGET := all-gcc all-target-libgcc
MAKE_INSTALL_TARGET := install-gcc install-target-libgcc


BASE_CONFIGURE_FLAGS := \
    --disable-nls 			\
    --disable-shared 			\
    --disable-static 			\
    --disable-decimal-float		\
    --disable-libgomp			\
    --disable-libmudflap		\
    --disable-libssp			\
    --disable-threads			\
    --disable-multilib			\
    --enable-cloog-backend=isl		\
    --enable-languages=c,c++		\
    --with-cloog=__SYSROOT__/../usr	\
    --with-gmp-prefix=__SYSROOT__/../usr	\
    --with-gnu-as			\
    --with-gnu-ld			\
    --with-mpfr-prefix=__SYSROOT__/../usr	\
    --with-newlib			\
    --with-ppl=__SYSROOT__/../usr	\
    --without-headers			\
    $(NULL)

CONFIGURE_FLAGS := $(BASE_CONFIGURE_FLAGS)


include $(CURDIR)/asacos-build/exec.mk

MODULE := gcc_1_arm_eabi
TARGET := arm-asac-linux-gnueabi
REQ := binutils_1_arm_eabi

CONFIGURE_FLAGS :=  $(BASE_CONFIGURE_FLAGS)	\
	--with-arch=armv7-a			\
	--with-tune=cortex-a9			\
	--with-fpu=vfpv3-d16			\
	--with-float=softfp			\
	$(NULL) 

include $(CURDIR)/asacos-build/exec.mk

# virtual "all binutils" settings
MODULE := gcc_1
CLASS := NOP
REQ := $(foreach arch, i586_elf arm_eabi, gcc_1_$(arch))


# 2nd stage
include $(CURDIR)/asacos-build/exec.mk
include $(CURDIR)/asacos-build/reset.mk

MODULE := gcc_2_i586_elf
TARGET := i586-pc-linux-gnu
PRODUCT := toolchain
CLASS := host
CAT := xtoolchain
TYPE := autoconf
SRC_NAME := gcc
REQ := glibc_1_i586_elf
MAKE_TARGET :=
MAKE_INSTALL_TARGET := install
C_ENVS := AR=ar LD=ld
M_VARS := AS_FOR_TARGET="$(TARGET)-as" LD_FOR_TARGET="$(TARGET)-ld"

BASE_CONFIGURE_FLAGS := \
    --disable-multilib		\
    --disable-nls		\
    --disable-static		\
    --enable-__cxa_atexit \
    --enable-c99 \
    --enable-cloog-backend=isl		\
    --enable-languages=c,c++	\
    --enable-long-long \
    --enable-shared		\
    --enable-threads=posix	\
    --with-cloog=__SYSROOT__/../usr	\
    --with-gmp-prefix=__SYSROOT__/../usr	\
    --with-gnu-as			\
    --with-gnu-ld			\
    --with-mpfr-prefix=__SYSROOT__/../usr	\
    --with-ppl=__SYSROOT__/../usr	\
    $(NULL)

CONFIGURE_FLAGS := $(BASE_CONFIGURE_FLAGS)

include $(CURDIR)/asacos-build/exec.mk

MODULE := gcc_2_arm_eabi
TARGET := arm-asac-linux-gnueabi
REQ := glibc_1_arm_eabi

CONFIGURE_FLAGS :=  $(BASE_CONFIGURE_FLAGS)	\
	--with-arch=armv7-a			\
	--with-tune=cortex-a9			\
	--with-fpu=vfpv3-d16			\
	--with-float=softfp			\
	$(NULL)

include $(CURDIR)/asacos-build/exec.mk

# virtual "all binutils" settings
MODULE := gcc_2
CLASS := NOP
REQ := $(foreach arch, i586_elf arm_eabi, gcc_2_$(arch))

