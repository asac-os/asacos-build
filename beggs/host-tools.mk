#!/usr/bin/make -f
#
# Copyright (C) 2012  Alexander Sack <asac@jwsdot.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
MODULE := ht_file
SRC_NAME := file
PRODUCT := toolchain
CLASS := host
CAT := xtools
TYPE := autoconf
REQ :=
CONFIGURE_FLAGS :=
include $(CURDIR)/asacos-build/exec.mk

MODULE := ht_m4
SRC_NAME := m4
PRODUCT := toolchain
REQ :=
CONFIGURE_FLAGS :=
include $(CURDIR)/asacos-build/exec.mk

MODULE := ht_ncurses
SRC_NAME := ncurses
PRODUCT := toolchain
REQ :=
CONFIGURE_FLAGS :=	\
    --without-debug --without-shared
include $(CURDIR)/asacos-build/exec.mk

MODULE := ht_gmp
SRC_NAME := gmp
PRODUCT := toolchain
REQ :=
CONFIGURE_FLAGS :=	\
    --enable-cxx --disable-static
include $(CURDIR)/asacos-build/exec.mk

MODULE := ht_mpfr
SRC_NAME := mpfr
PRODUCT := toolchain
REQ := ht_gmp
CONFIGURE_FLAGS :=	\
    --enable-shared --disable-static --with-gmp=__SYSROOT__/../usr
include $(CURDIR)/asacos-build/exec.mk

MODULE := ht_mpc
SRC_NAME := mpc
PRODUCT := toolchain
REQ := ht_gmp ht_mpfr
CONFIGURE_FLAGS :=	\
    --disable-static --with-gmp=__SYSROOT__/../usr --with-mpfr=__SYSROOT__/../usr
include $(CURDIR)/asacos-build/exec.mk

MODULE := ht_ppl
SRC_NAME := ppl
PRODUCT := toolchain
REQ := ht_gmp
CONFIGURE_FLAGS :=	\
    --disable-static --with-gmp=__SYSROOT__/../usr
include $(CURDIR)/asacos-build/exec.mk

MODULE := ht_cloog
SRC_NAME := cloog
PRODUCT := toolchain
REQ := ht_gmp ht_ppl
CONFIGURE_FLAGS :=	\
    --enable-shared --disable-static --with-gmp-prefix=__SYSROOT__/../usr
include $(CURDIR)/asacos-build/exec.mk


# all tools NOP module
MODULE := ht
CLASS := NOP
REQ := \
    ht_file	\
    ht_m4 	\
    ht_ncurses 	\
    ht_gmp 	\
    ht_mpfr 	\
    ht_mpc 	\
    ht_ppl	\
    ht_cloog 	\
    $(NULL)
