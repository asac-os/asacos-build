#!/usr/bin/make -f
#
# Copyright (C) 2012  Alexander Sack <asac@jwsdot.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
MODULE := binutils_1_i586_elf
TARGET := i586-pc-linux-gnu
PRODUCT := toolchain
CLASS := host
CAT := xtoolchain
TYPE := autoconf
SRC_NAME := binutils
REQ := ht
C_ENVS := AS=as AR=ar

CONFIGURE_FLAGS := 		\
    --enable-shared		\
    --disable-multilib		\
    --disable-nls		\
    --disable-static		\
    --disable-werror		\
    $(NULL)

include $(CURDIR)/asacos-build/exec.mk

MODULE := binutils_1_arm_eabi
TARGET := arm-asac-linux-gnueabi
REQ := ht

include $(CURDIR)/asacos-build/exec.mk

# virtual "all binutils" settings
MODULE := binutils_1
CLASS := NOP
REQ := $(foreach arch, i586_elf arm_eabi, binutils_1_$(arch))
