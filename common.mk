#!/usr/bin/make -f
#
# Copyright (C) 2012  Alexander Sack <asac@jwsdot.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
OO ?= out/
ABS_O = $(shell mkdir -p $(OO); cd $(OO); pwd)
TARGET_PRODUCT ?= asaccore
TARGET_FLAVOUR ?= eng
TARGET_PLATFORM ?= i586
TARGET ?= i386-pc-linux-gnu
exec_MAKEFLAGS_J ?= 10
MODULE_CLASS ?= autoconf-product

MAKE_INSTALL_TARGET ?= install

# flavours = eng prod
# products = amd64 i386
#$(foreach a,$(flavours),$(foreach b,$(products),$(b)-$(a))):
#	make TARGET_PRODUCT=$$(echo $@ | sed -e 's/.*-//') TARGET_FLAVOUR=$$(echo $@ | sed -e 's/-.*//') build
